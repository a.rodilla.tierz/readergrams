import sys
from os import path

def gramCutter(textManage, n_gram, workBlock):
    for nodeWord in textManage.split(' '):
        workBlock.append(nodeWord)
        if len(workBlock) == n_gram:
            print ' '.join(workBlock)
            workBlock = []

    return workBlock

def gramManager(arg1, arg2):
    workBlock = []
    with open(arg2, 'r') as content_file:
        for f in content_file:
             workBlock = gramCutter(f, int(arg1),  workBlock)

def validateArguments(arg1, arg2):
    pathUse = None
    gramNum = 0
    if path.exists(arg2):
        pathUse = arg2
    else:
        print 'file unvalid: ', arg2

    try:
        gramNum = int(arg1)
    except ValueError:
      print ("Please enter a valid number on argument 1: ", arg1)

    if pathUse != None and gramNum > 0:
        gramManager(gramNum, pathUse)

validateArguments(sys.argv[1], sys.argv[2])
